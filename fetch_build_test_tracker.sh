#!/bin/bash

set -eu

git clone https://gitlab.gnome.org/GNOME/tracker.git
cd tracker
meson . build -Db_lto=true -Ddocs=false -Dsystemd_user_services=false -Dtests_tap_protocol=true --prefix /usr -Doverride_sqlite_version_check=true
ninja -C build
env LANG=C.UTF-8 LC_ALL=C.UTF-8 dbus-run-session meson test -C build --print-errorlogs
