local param_job(tag) =
  {
    image: "registry.gitlab.gnome.org/gnome/tracker/fedora/34:x86_64-2021-06-13.2",
    cache: { key: "$CI_JOB_NAME", paths: [ "./sqlite-downloads/" ] },
    artifacts: {
      paths: [ "./tracker/build/meson-logs/" ]
    },
    script: [
      "sudo dnf install -y tcl",
      "mkdir -p sqlite-downloads sqlite-build",
      "echo \"Installing SQLite " + tag + " into /usr\"",
      "bash ./fetch_build_install_sqlite.sh " + tag + " ./sqlite-downloads ./sqlite-build /usr",
      "echo \"Running Tracker test suite\"",
      "bash ./fetch_build_test_tracker.sh"
    ]
  };


local main(versions_text) =
  local versions_array = std.prune([
    version for version in std.split(versions_text, "\n")
    if !std.startsWith(version, "#") && std.length(version) > 0
  ]);
  {
    ["sqlite-" + version]: param_job(version)
    for version in versions_array
  };

main
